# -*- coding: utf-8 -*-

import math
def Error(message):
	print "Error: "+message
	exit()

def filtre(tab, x, fil):
	""" retourne les valeurs de tab dont le x correspondant vaut fil """
	result = []
	for i in range(len(x)):
		if x[i]==fil:
			result.append(tab[i])
	return result

# Lecture, Ecriture, affichage et debug ------------------------------
def ecrire(x, y, name):
	if len(x) != len(y):
		Error("ecriture impossible: taille de x different de y")
	print "ecriture du fichier" + name
	myFile = open(name, "w")
	for i in range(len(x)):
		myFile.write(str(x[i]) + " " + str(y[i]) + "\n")
	print "fichier ecrit"

def lineToVector(line):
	vector = line.split(" ")
	newVector = []
	for elem in vector:
		if elem != '':
			newVector.append(float(elem))
	return newVector

def ecrireHistoSh(names):
	fichier = open("histo.sh", "w")
	fichier.write("#!/bin/bash\n\ngnuplot << EOP\n\n")
	for name in names:
		fichier.write("reset\n")
		fichier.write("set term png truecolor\n")
		fichier.write("set output \""+name+".png\"\n")
		fichier.write("set style data histogram\n")
		fichier.write("set grid x y\n")
		fichier.write("set xlabel \"Classe (intervale)\"\n")
		fichier.write("set ylabel \"Nombre d'échantillons\"\n")
		fichier.write("set grid\n")
		fichier.write("set style fill transparent solid 0.5 noborder\n")
		#fichier.write("binwidth=5\n")
		#fichier.write("bin(x,width)=width*floor(x/width)\n")
		fichier.write("plot \"data/"+name+"\" u 1:2 w boxes lc rgb\"green\" notitle\n")# using (bin(5,binwidth)):(1.0) smooth freq with boxes\n\n")
	fichier.write("EOP")
	fichier.close()
		

def getFileColumn(filename):
	cy = []
	c1 = []
	c2 = []
	c3 = []
	print "lecture du fichier" + filename
	myFile = open(filename, "r")
	for line in myFile.readlines():
		vect = lineToVector(line)
		cy.append(vect[0])
		c1.append(vect[1])
		c2.append(vect[2])
		c3.append(vect[3])
	myFile.close()
	print "fichier lu"
	return [cy, c1, c2, c3]

def afficher(tab, n=-1):
	print "----------"
	counter = 0
	for number in tab:
		if counter == n:
			return
		print number
		counter = counter+1

def afficheLigne(ligne):
	print ": "+ ','.join(str(e) for e in ligne)
# Fonctions probabilité ------------------------------
def moyenne(tab):
	""" retourne la moyenne des valeurs dans tab """
	sum = 0;
	for nombre in tab:
		sum = sum + nombre
	return sum/ len(tab)

def getVariance(tab, moyenne):
	""" retourne la variance des valeurs dans tab """
	sum = 0
	for nombre in tab:
		sum = sum + (nombre-moyenne)**2
	return sum/ len(tab)

def correlation(X , Y):
	""" retourne un tableau dont le premier element est a, 2eme est b et 3eme est le Rcarre """
	n = len(X)
	if n != len(Y):
		Error("problem, len(X) different de len(Y)")
	rangen = range(n)
	sumxi = 0
	for i in rangen:
		sumxi = sumxi + X[i]
	sumyi = 0
	for i in rangen:
		sumyi = sumyi + Y[i]
	sumxiyi = 0
	for i in rangen:
		sumxiyi = sumxiyi + ( X[i] * Y[i] )
	sumxiSquare = 0
	for i in rangen:
		sumxiSquare = sumxiSquare + X[i]**2
	#calcul de a et b : la regression lineaire
	denominator = float(n*sumxiSquare - sumxi**2)
	a = (n*sumxiyi - sumxi*sumyi)/denominator
	b = (sumyi*sumxiSquare - sumxi*sumxiyi)/denominator
	#donc yReg(x) = a*x+b . calcul de R carre
	moyenneY = moyenne(Y)
	numerator = 0
	denominator = 0
	for i in rangen:
		numerator = numerator + ((a*X[i] +b) - moyenneY)**2
		denominator = denominator + (Y[i] - moyenneY)**2
	R2 = numerator/float(denominator)
	return [a, b, R2]

def getRepartitions(tab, interval):
	""" Retourne les classes de taille 'interval' dans lesquelles on pourra distribuer les echantillons dans tab 
	 et Retourne la repartitions des echantillons dans tab dans les classes se trouvant dans classes """
	 #classes[i] = classe de histY[i]
	classes = []
	for data in tab:
		classes.append( ((int)(data/interval))+(interval/2) )
	#obtenir classe maximale
	maxClass = 0
	for c in classes:
		if c>maxClass:
			maxClass = c
	#obtenir classe minimale
	minClass = maxClass
	for c in classes:
		if c<minClass:
			minClass = c
	#tableau des classes
	x = []
	for i in range((int)(maxClass+1-minClass)):
		x.append(minClass+i)
	#compter nombres d echantillons par classe
	y = []
	for i in x:
		y.append(0)
	for c in classes:
		indice = (int)(c-minClass)
		y[indice] += 1
	return [x, y]

def tracerHistogramme(repartition, name):
	""" Ecrit le fichier de donnees representant l'histogramme des donnees dans la repartition """
	#ecrire
	ecrire(repartition[0] , repartition[1], name)

def getTailles(x):
	""" retourne les différentes tailles disponible. x est la 1ere colonne des données de l'énoncé """
	tailles = []
	prevTaille = -1
	for t in x:
		if t!=prevTaille:
			prevTaille = t
			tailles.append(t)
	return tailles

def ecrireHistogramme(x, c1, c2, c3, interval):
	""" Script principale traçant l'histogramme de toutes les donnees de l'enonce """
	print "traçage des histogrammes"
	names=[]
	#les tailles differentes
	tailles = getTailles(x)
	#pour chaque tailles
	for t in tailles:
		#pour chaque colonne, filtrer les donnees pour t et tracer histrogramme
		histY = filtre(c1, x, t)
		name = "histogrammeC1-"+str(t)+".dat"
		names.append(name)
		repartition = getRepartitions(histY, interval)
		tracerHistogramme(repartition, name)
		histY = filtre(c2, x, t)
		name = "histogrammeC2-"+str(t)+".dat"
		names.append(name)
		repartition = getRepartitions(histY, interval)
		tracerHistogramme(repartition, name)
		histY = filtre(c3, x, t)
		name = "histogrammeC3-"+str(t)+".dat"
		names.append(name)
		repartition = getRepartitions(histY, interval)
		tracerHistogramme(repartition, name)
	ecrireHistoSh(names)

def loiNormale(x, variance, mu):
	return (1/math.sqrt(2*math.pi*variance))*math.exp((-(x-mu)**2)/(2*variance))

def kolmogorov(tab, x, moyenne, variance, tclasse):
	""" retourne Dn par la formule de Kolmogorov-Smirnov pour une loi de distribution gaussienne
	tab contients le nombre réelle d'echantillons par classe
	x est le tableau de classes """
	#nombre total d echantillon
	tot = 0.0
	for nombre in tab:
		tot = tot+nombre
	sup = 0
	Fn = 0
	Fo = 0
	#initialiser komolgorov
	niter = (int)(round(( x[0]-(tclasse/2) )/tclasse))
	for i in range(niter):
		Fo = Fo + tot*loiNormale((i*tclasse)+(tclasse/2), variance, moyenne)
	if len(tab)!=len(x):
		Error("kolmogorov: taille de tab different de celui de x\n")
	for i in range(len(tab)):
		theo = tot*loiNormale(x[i], variance, moyenne)
		Fn = Fn + theo*tclasse
		Fo = Fo + tab[i]
		delta = math.fabs(Fn-Fo)
		#print "le delta de Fn="+str(Fn)+" et Fo="+str(Fo)+" est "+str(delta)
		if sup<delta:
			sup = delta
	return sup/(float)(tot)

def testKolmogorov(tab, x, moyenne, variance, tclasse):
	""" retourne si Vrai si on accepte l'hypothèse nulle avec le test de Kolmogorov-Smirnov pour une loi de distribution gaussienne avec n=100 et alpha=0.05 """
	Dn = kolmogorov(tab, x, moyenne, variance, tclasse)
	Dalpha = 0.134
	if Dn<Dalpha:
		print "Kolmogorov ok. Dn="+str(Dn)+" et Dalpha="+str(Dalpha)
	else:
		print "FAIL! Kolmogorov non ok. Dn="+str(Dn)+" et Dalpha="+str(Dalpha)
	return Dn<Dalpha

def testsKolmogorov(x, c, tclasse):
	""" Compte le nombre de tests echouant au test de Kolmogorov pour la carte c """
	tailles = getTailles(x)
	countFail = 0
	for t in tailles:
		column = filtre(C1, X, t)
		repartitions = getRepartitions(column, tclasse)
		mu = moyenne(column)
		variance = getVariance(column, mu)
		if testKolmogorov(repartitions[1], repartitions[0], mu, variance, tclasse)==False:
			countFail = countFail + 1
	#print str(countFail)+" tests de Kolmogorov-Sminorv ratés sur "+str(len(tailles))+" = "+str(100*(1-(((float)(countFail))/len(tailles))))+"% de réussites"
	return countFail

def testAllKolmogorov(x, c1, c2, c3, tclasse):
	""" Test de kolmogorov sur toutes les colonnes avec alpha = 0.05"""
	count = testsKolmogorov(x,c1, tclasse) + testsKolmogorov(x,c2, tclasse) + testsKolmogorov(x,c3, tclasse)
	print "        #### Résultat Kolmogorov ####"
	print str(count)+" Tests ratés sur un total de "+str(len(3*getTailles(x)))+"         = "+str(100*(1-(((float)(count))/(3*len(getTailles(x))))))+"% de fiabilité"
	print " --- "
	return count

def khi(tab, x, moyenne, variance):
	""" Test de Khi carré pour une loi de distribution gaussienne (normale) où tab est le tableau de nombre d'échantillon par classes et x les classes correspondantes """
	if len(tab)!=len(x):
		Error("testKhi2: taille de tab différent de celui de x\n")
	tot = 0
	for nombre in tab:
		tot = tot+nombre
	sum = 0.0
	for i in range(len(tab)):
		pi = (float)(loiNormale(x[i], variance,moyenne))
		sum = sum + ((tab[i]-tot*pi)**2)/(tot*pi)
	return sum

def getXcarre(r):
	""" retourne la valeur de Xcarre pour r degrés de liberté pour alpha = 0.05 """
	fracts = [3.841, 5.991, 7.815, 9.488, 11.070, 12.592, 14.067, 15.507, 16.919, 18.307, 19.675, 21.026, 22.362, 23.685, 24.996, 26.296, 27.587, 28.869, 30.144, 31.410, 32.671, 33.924, 35.172, 36.415, 37.652, 38.885, 40.113, 41.337, 42.557, 43.773, 44.985, 46.194]
	if r > len(fracts):
		Error("valeur inconne de Xcarre pour "+str(r)+" degrés de liberté")
	return fracts[r-1]

def testKhi(tab, x, moyenne, variance):
	""" applique le test de Khi carré sur tab dont les classes correspondantes sont dans x et compare avec Xcarré pour alpha=0.05 """
	Xcarre = getXcarre(len(x)-1)
	Kn = khi(tab, x, moyenne, variance)
	if Kn < Xcarre:
		print "Khi2 ok. Kn="+str(Kn)+" et X2ralpha="+str(Xcarre)
	else:
		print "FAIL! Khi2 non ok. Kn="+str(Kn)+" et X2ralpha="+str(Xcarre)
	return Kn<Xcarre

def testsKhi(x, c):
	""" Compte le nombre de test Khi carré qui échouent pour la carte c. Il test si la loi de distribution est une loi normale """
	tailles = getTailles(x)
	countFail = 0
	for t in tailles:
		column = filtre(C1, X, t)
		repartitions = getRepartitions(column, tclasse)
		mu = moyenne(column)
		variance = getVariance(column, mu)
		if testKhi(repartitions[1], repartitions[0], mu, variance)==False:
			countFail = countFail + 1
	#print str(countFail)+" tests de Khi2 ratés sur "+str(len(tailles))+"         = "+str(100*(1-(((float)(countFail))/len(tailles))))+"% de réussites"
	return countFail

def testAllKhi(x, c1, c2, c3):
	""" Test de Khi2 sur toutes les colonnes avec alpha = 0.05"""
	count = testsKhi(x,c1) + testsKhi(x,c2) + testsKhi(x,c3)
	print "        #### Résultat Khi2 ####"
	print str(count)+" Tests ratés sur un total de "+str(len(3*getTailles(x)))+" = "+str(100*(1-(((float)(count))/(3*len(getTailles(x))))))+"% de fiabilité"
	print " --- "
	return count

def khiAdmissible(tab):
	""" Test si il y a au moins 4 échantillons par classe. tab est le nombre d'échantillons par classe """
	for nombre in tab:
		if nombre < 4:
			return False
	return True

def testsKhiAdmissible(x, c):
	""" Retourne le poucentage de fiabilité du test Khi2 """
	tailles = getTailles(x)
	countFail = 0
	for t in tailles:
		column = filtre(C1, X, t)
		repartitions = getRepartitions(column, tclasse)
		if khiAdmissible(repartitions[1])==False:
			countFail = countFail + 1
	print str(100*(1-(((float)(countFail))/len(tailles))))+"% de fiabilité pour le test de Khi2"
	return countFail

def affMuVar(x,c1, c2, c3):
	""" affiches moyennes et variances """
	tailles = getTailles(x)
	fichier = open("tabMuVar.txt", "w")
	fichier.write("\\begin{tabular}{c|l|l|l|l|l|l}\n")
	fichier.write("\\textbf{Taille} & \\textbf{$\\hat{\\mu}$ Carte1} & \\textbf{$\\hat{\\sigma^{2}}$ Carte1} & \\textbf{$\\hat{\\mu}$ Carte2} & \\textbf{$\\hat{\\sigma^{2}}$ Carte2} & \\textbf{$\\hat{\\mu}$ Carte3} & \\textbf{$\\hat{\\sigma^{2}}$ Carte3} \\\\ \hline\n")
	for t in tailles:
		column1 = filtre(c1, x, t)
		column2 = filtre(c2, x, t)
		column3 = filtre(c3, x, t)
		m1 = moyenne(column1)
		m2 = moyenne(column2)
		m3 = moyenne(column3)
		fichier.write(str(t)+" & "+str(round(m1,4))+" & "+str(round(getVariance(column1,m1),4))+" & "+str(round(m2,4))+" & "+str(round(getVariance(column2,m2),4))+" & "+str(round(m3,4))+" & "+str(round(getVariance(column3,m3),4))+"\\\\\n")
	fichier.write("\\end{tabular}")
	fichier.close()
	
# Script principal ------------------------------

filename = "Q15_donnee.dat"
vect = getFileColumn(filename)
X = vect[0]
C1 = vect[1]
C2 = vect[2]
C3 = vect[3]
alpha = 0.05
MinR2 = 0.9
tclasse = 1.0

ecrireHistogramme(X, C1, C2, C3, tclasse)

print " Moyennes et variances ------------------ "
affMuVar(X,C1,C2,C3)

print "test de kolmogorov-Smirnov ----------------"
testAllKolmogorov(X,C1,C2,C3, tclasse)

print "test d'admissibilité pour le test de Khi2 ------"
print " --- colonne 1 --- "
testsKhiAdmissible(X,C1)
print " --- colonne 2 --- "
testsKhiAdmissible(X,C2)
print " --- colonne 3 --- "
testsKhiAdmissible(X,C3)

print "test de Khi2 ---------------------"
testAllKhi(X,C1,C2,C3)

print "calcul des corrélations pour chaque carte réseau ------------------"
corre1 = correlation(X,C1)
corre2 = correlation(X,C2)
corre3 = correlation(X,C3)
print "Maintenant il faut vérifier si ces correlations sont fiables grâce à la valeur de R carré."
print "R2 de la carte réseau 1 = " +str(corre1[2])
print "R2 de la carte réseau 2 = " +str(corre2[2])
print "R2 de la carte réseau 3 = " +str(corre3[2])

print "On va supposer que la correlation est fiable pour un R carré supérieur à " + str(MinR2)
if corre1[2]<MinR2 :
	print "La correlation n'est pas fiable pour la carte réseau 1. On abandonne l'idée"
elif corre2[2]<MinR2 :
	print "La correlation n'est pas fiable pour la carte réseau 2. On abandonne l'idée"
elif corre3[2]<MinR2 :
	print "La correlation n'est pas fiable pour la carte réseau 3. On abandonne l'idée"
else:
	print "La correlation de chaque carte réseau est fiable. Voici les corrélations à comparer:"
	print "Carte 1:  y(x) = " + str(corre1[0]) +"x + " + str(corre1[1])
	print "Carte 2:  y(x) = " + str(corre2[0]) +"x + " + str(corre2[1])
	print "Carte 3:  y(x) = " + str(corre3[0]) +"x + " + str(corre3[1])
	#ax+b = cx+d ; ax-cx = d-b = x(a-c) = d-b ; x = (d-b)/(a-c) ; y = ax+b
	x = (corre2[1]-corre1[1])/float((corre1[0]-corre2[0]))
	y = corre1[0]*x + corre1[1]
	print "Interesection entre la correlation de la carte 1 et celle de la carte 2: ("+str(x)+" , "+str(y)+")"
	x = (corre3[1]-corre2[1])/float((corre2[0]-corre3[0]))
	y = corre2[0]*x + corre2[1]
	print "Interesection entre la correlation de la carte 2 et celle de la carte 3: ("+str(x)+" , "+str(y)+")"
	x = (corre1[1]-corre3[1])/float((corre3[0]-corre1[0]))
	y = corre3[0]*x + corre3[1]
	print "Interesection entre la correlation de la carte 3 et celle de la carte 1: ("+str(x)+" , "+str(y)+")"
