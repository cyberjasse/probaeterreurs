#!/bin/bash

gnuplot << EOP

algo1 = "column1.dat"
algo2 = "column2.dat"
algo3 = "column3.dat"

set terminal jpeg size 640,480
set output "all.jpg"
set grid x y
set xlabel "Moment of the Day"
set ylabel "Time(s)"

plot algo1, \
     algo2, \
     algo3

EOP
