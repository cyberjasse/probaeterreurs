#!/bin/bash

gnuplot << EOP

echantillon = "data/column1.dat"

set terminal jpeg font arial 8 size 640,480
set output "carte1.jpg"
set grid x y
set xlabel "Taille du packet Mbit"
set ylabel "Temps de transfert s"
set key on inside left top
set title "Carte 1"

plot echantillon

echantillon = "data/column2.dat"

set terminal jpeg font arial 8 size 640,480
set output "carte2.jpg"
set grid x y
set xlabel "Taille du packet Mbit"
set ylabel "Temps de transfert s"
set key on inside left top
set title "Carte 2"

plot echantillon

echantillon = "data/column3.dat"

set terminal jpeg font arial 8 size 640,480
set output "carte3.jpg"
set grid x y
set xlabel "Taille du packet Mbit"
set ylabel "Temps de transfert s"
set key on inside left top
set title "Carte 3"

plot echantillon

echantillon_Carte1 = "data/column1.dat"
echantillon_Carte2 = "data/column2.dat"
echantillon_Carte3 = "data/column3.dat"
set terminal jpeg font arial 8 size 640,480
set output "cartes.jpg"
set grid x y
set xlabel "Taille du packet Mbit"
set ylabel "Temps de transfert s"
set key on inside left top
set title "Echantillons provenant des 3 cartes réseaux"
plot echantillon_Carte1 lt rgb "red", \
	echantillon_Carte2 lt rgb "green", \
	echantillon_Carte3 lt rgb "blue"

intersection = "data/intersection.dat"
set terminal jpeg font arial 8 size 2560,1440
set output "correlations.jpg"
set grid x y
set xlabel "Taille du packet Mbit"
set ylabel "Temps de transfert s"
set key on inside left top
set title "Corrélations des 3 cartes réseaux"
set style line 10 linetype 1 \
	linecolor rgb "#5080cb" \
	linewidth 1
set style line 11 linetype 1 \
	linecolor rgb "#10bb70" \
	linewidth 1
set style line 12 linetype 1 \
	linecolor rgb "#30bbcb \
	linewidth 1
plot 0.4980*x + 0.0365 ls 10, \
	0.5105*x -0.0945 ls 11, \
	0.5045*x -0.2508 ls 12, \
	intersection

EOP
