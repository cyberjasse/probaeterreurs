#!/bin/bash

gnuplot << EOP

reset
set term png truecolor
set output "histogrammeC1-10.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-10.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-10.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-10.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-10.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-10.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-20.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-20.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-20.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-20.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-20.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-20.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-30.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-30.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-30.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-30.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-30.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-30.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-40.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-40.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-40.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-40.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-40.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-40.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-50.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-50.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-50.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-50.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-50.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-50.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-60.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-60.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-60.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-60.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-60.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-60.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-70.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-70.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-70.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-70.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-70.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-70.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-80.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-80.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-80.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-80.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-80.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-80.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-90.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-90.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-90.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-90.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-90.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-90.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC1-100.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC1-100.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC2-100.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC2-100.0.dat" u 1:2 w boxes lc rgb"green" notitle
reset
set term png truecolor
set output "histogrammeC3-100.0.dat.png"
set style data histogram
set grid x y
set xlabel "Classe (intervale)"
set ylabel "Nombre d'échantillons"
set grid
set style fill transparent solid 0.5 noborder
plot "data/histogrammeC3-100.0.dat" u 1:2 w boxes lc rgb"green" notitle
EOP