#!/bin/bash

gnuplot << EOP

reset
set term png truecolor
set output "algo3n50.png"
set xlabel "Interval"
set ylabel "Quantité"
set grid
set style fill transparent solid 0.5 noborder
plot "algo3n50.dat" u 1:2 w boxes lc rgb"green" notitle
EOP
